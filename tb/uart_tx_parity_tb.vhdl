library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library uart;
use uart.uart_pkg.all;

entity uart_tx_parity_tb is
    generic (runner_cfg : string);
    constant CLOCK_FREQUENCY : natural := 100_000_000;
    constant BAUD_RATE : natural := 115_200;
    constant DATA_WIDTH : natural := 8;
    constant STOP_BITS : natural := 1;
    constant PARITY_EN : natural := 1;
    constant PARITY_MODE : parity_mode_t := odd;
end entity;

architecture testbench of uart_tx_parity_tb is
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    constant PACKET_WIDTH : natural := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    constant BAUD_PERIOD : time := (1.0/real(BAUD_RATE)) * 1 sec;
    constant PACKET_PERIOD : time := PACKET_WIDTH * BAUD_PERIOD;
    ---------------------------------------------------------------------------
    signal reset_n : std_logic;
    signal clock : std_logic := '0';
    signal tx_start : std_logic := '1';
    signal tx_data : std_logic_vector((DATA_WIDTH - 1) downto 0);
    signal tx_pin : std_logic;
    signal tx_done : std_logic;
    ---------------------------------------------------------------------------
    -- Preserves previous tx_data before changing
    signal previous_tx_data : std_logic_vector((DATA_WIDTH - 1) downto 0);
    signal tx_data_expected : std_logic_vector((PACKET_WIDTH - 1) downto 0);
    signal transaction_done : std_logic;
    signal parity_signal : std_logic;
begin
    ---------------------------- DUT Instantiation ----------------------------
    dut : entity uart.uart_tx generic map(
        CLOCK_FREQUENCY => CLOCK_FREQUENCY,
        BAUD_RATE => BAUD_RATE,
        DATA_WIDTH => DATA_WIDTH,
        STOP_BITS => STOP_BITS,
        PARITY_EN => PARITY_EN,
        PARITY_MODE => PARITY_MODE
        )
        port map(
            reset_n => reset_n,
            clock => clock,
            tx_start => tx_start,
            tx_data => tx_data,
            tx_pin => tx_pin,
            tx_done => tx_done
        );
    --------------------------- Stimuli Generation ----------------------------
    clock <= not clock after ((1.0/real(CLOCK_FREQUENCY * 2)) * 1 sec);
    reset_n <= '0', '1' after 10 ns;
    tx_start <= not tx_start after (PACKET_PERIOD/2);
    -----------------------------------
    stimuli_generator : process begin
        wait until reset_n;

        -- Send data from 0 - ((2 ** DATA_WIDTH) - 1), one at a time
        for i in 0 to ((2 ** DATA_WIDTH) - 1) loop
            -- Writing the tx_data signal begins the driver process
            tx_data <= std_logic_vector(to_unsigned(i, DATA_WIDTH));
            wait until tx_done;
        end loop;
    end process;
    -----------------------------------
    -- Store data from the tx_pin
    driver : process
        variable expected : std_logic_vector((PACKET_WIDTH - 1) downto 0) := (others => '0');
    begin
        wait on tx_data'transaction;
        wait until tx_pin = '0';
        -- Preserves tx_data
        previous_tx_data <= tx_data;

        for i in 0 to PACKET_WIDTH - 1 loop
            -- Shift tx_pin in expected
            expected := tx_pin & expected((PACKET_WIDTH - 1) downto 1);
            wait on tx_pin'transaction;
        end loop;

        tx_data_expected <= expected;
        transaction_done <= '1';
    end process;
    -----------------------------------
    parity_generator : process
        variable tx_parity : std_logic;
    begin
        wait on tx_data'transaction;

        if PARITY_EN = 1 then
            if PARITY_MODE = odd then
                tx_parity := '1';
                for i in tx_data'range loop
                    tx_parity := tx_parity xor tx_data(i);
                end loop;

            elsif PARITY_MODE = even then
                tx_parity := '0';
                for i in tx_data'range loop
                    tx_parity := tx_parity xor tx_data(i);
                end loop;

            elsif PARITY_MODE = mark then
                tx_parity := '1';

            elsif PARITY_MODE = space then
                tx_parity := '0';
            end if;
            parity_signal <= tx_parity;
        else
            parity_signal <= '0';
        end if;
    end process;
    ---------------------------------- Check ----------------------------------
    test_runner : process
        variable stop_bits_value : std_logic_vector(STOP_BITS - 1 downto 0) := (others => '1');
    begin
        test_runner_setup(runner, runner_cfg);

        wait until reset_n;
        for i in 0 to ((2 ** DATA_WIDTH) - 1) loop
            wait on transaction_done'transaction;
            -- Check that all values of tx_data are tested
            check_equal(previous_tx_data, i, "Didn't test all values");
            -- Print the UART frame on the screen
            print("start: " & to_string((tx_data_expected(0))) & "    " &

            "stop: " & to_string(
            tx_data_expected((PACKET_WIDTH - 1) downto (PACKET_WIDTH - STOP_BITS))) & "    " &

            to_string(PARITY_MODE) & "_parity: " & to_string(tx_data_expected(DATA_WIDTH + 1)) & ht &

            "data: " & to_string(tx_data_expected(DATA_WIDTH downto 1)) & " : " &
            to_string(to_integer(unsigned((tx_data_expected(DATA_WIDTH downto 1)))))
            );
            -- Check start value
            check_equal(tx_data_expected(0), '0', "Wrong Start value");
            -- Compare the actual and expected values of the tx_pin 
            check_equal(tx_data_expected(DATA_WIDTH downto 1), previous_tx_data,
            "tx_data is " & to_string(previous_tx_data));
            -- Check for parity error
            check_equal(tx_data_expected(DATA_WIDTH + 1), parity_signal, "Wrong parity");
            -- Check correct stop bits value
            check_equal(tx_data_expected((PACKET_WIDTH - 1) downto (PACKET_WIDTH - STOP_BITS)),
            stop_bits_value, "wrong stop bit value");
        end loop;

        info("End of testbench. All tests passed.");
        test_runner_cleanup(runner);
    end process;
    -- Set it to a higher value if you get timeout error
    test_runner_watchdog(runner, PACKET_PERIOD * ((2 ** DATA_WIDTH) + 1));
end architecture;