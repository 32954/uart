library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library uart;
use uart.uart_pkg.all;

entity uart_rx_tb is
    generic (runner_cfg : string);
    constant CLOCK_FREQUENCY : natural := 100_000_000;
    constant BAUD_RATE : natural := 115_200;
    constant BAUD_SAMPLE : natural := 16;
    constant DATA_WIDTH : natural := 8;
    constant STOP_BITS : natural := 1;
    constant PARITY_EN : natural := 0;
    constant PARITY_MODE : parity_mode_t := odd;
end entity;

architecture testbench of uart_rx_tb is
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    constant PACKET_WIDTH : natural := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    constant BAUD_PERIOD : time := (1.0/real(BAUD_RATE)) * 1 sec;
    constant PACKET_PERIOD : time := PACKET_WIDTH * BAUD_PERIOD;
    ---------------------------------------------------------------------------
    signal reset_n : std_logic;
    signal clock : std_logic := '0';
    signal rx_pin : std_logic := '1';
    signal rx_data : std_logic_vector((DATA_WIDTH - 1) downto 0);
    signal rx_done : std_logic;
    ---------------------------------------------------------------------------
    signal rx_data_expected : std_logic_vector((DATA_WIDTH - 1) downto 0);
    signal transaction_done : std_logic;
begin
    ---------------------------- DUT Instantiation ----------------------------
    dut : entity uart.uart_rx generic map(
        CLOCK_FREQUENCY => CLOCK_FREQUENCY,
        BAUD_RATE => BAUD_RATE,
        BAUD_SAMPLE => BAUD_SAMPLE,
        DATA_WIDTH => DATA_WIDTH,
        STOP_BITS => STOP_BITS,
        PARITY_EN => PARITY_EN,
        PARITY_MODE => PARITY_MODE
        )
        port map(
            reset_n => reset_n,
            clock => clock,
            rx_pin => rx_pin,
            rx_data => rx_data,
            rx_done => rx_done
        );
    --------------------------- Stimuli Generation ----------------------------
    clock <= not clock after ((1.0/real(CLOCK_FREQUENCY * 2)) * 1 sec);
    reset_n <= '0', '1' after 20 ns;
    -----------------------------------
    data_generator : process begin
        wait until reset_n;

        -- Send data from 0 - ((2 ** DATA_WIDTH) - 1), one at a time
        for i in 0 to ((2 ** DATA_WIDTH) - 1) loop
            -- Writing the rx_data_expected signal begins the driver process
            rx_data_expected <= std_logic_vector(to_unsigned(i, DATA_WIDTH));
            -- Wait until the driver process has finished transmitting
            wait on transaction_done'transaction;
        end loop;
    end process;
    -----------------------------------
    -- Transmitting the value of rx_data_expected to rx_pin
    driver : process
        variable tx_buffer : std_logic_vector((PACKET_WIDTH - 1) downto 0);
        variable stop_signal : std_logic_vector((STOP_BITS - 1) downto 0) := (others => '1');
    begin
        wait on rx_data_expected'transaction;

        -- Add start and stop bits to rx_data_expected
        tx_buffer := stop_signal & rx_data_expected & '0';
        for i in tx_buffer'reverse_range loop
            rx_pin <= tx_buffer(i);
            wait for BAUD_PERIOD;
        end loop;
        transaction_done <= '1';
    end process;
    ---------------------------------- Check ----------------------------------
    test_runner : process
    begin
        test_runner_setup(runner, runner_cfg);

        wait until reset_n;
        for i in 0 to ((2 ** DATA_WIDTH) - 1) loop
            wait until rx_done;
            -- Check that all values of rx_data are tested
            check_equal(rx_data, i, "Didn't test all values");
            -- Compare the actual and expected values of the rx_data
            check_equal(rx_data, rx_data_expected, "rx_data is " & to_string(rx_data));
        end loop;

        info("End of testbench. All tests passed.");
        test_runner_cleanup(runner);
    end process;
    -- Set it to a higher value if you get timeout error
    test_runner_watchdog(runner, BAUD_PERIOD * PACKET_WIDTH * ((2 ** DATA_WIDTH) + 1));
end architecture;