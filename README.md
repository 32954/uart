# Generic UART With Parity

With this generic UART, you can specify the data size length, the number of stop bits and the type of parity (odd, even, mark, space).

Testbenches are written with the help of [VUnit](https://github.com/VUnit/vunit). To run them, type:

```bash
python run.py
```

or

```bash
python run.py -v
```

# How to use

`CLOCK_FREQUENCY`: clock frequency of your module in Hertz.

`BAUD_RATE`: baud rate of UART.

`BAUD_SAMPLE`: samples per Baud.

`DATA_WIDTH`: UART data frame size in bits.

`STOP_BITS`: number of stop bits.

`PARITY_EN`: enable or disable parity.

`PARITY_MODE`: parity type (odd, even, mark, space).

### **Clock Frequency:**

PACKET WIDTH = START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS

Each module's clock frequency is calculated as follows:

uart_rx's clock > PACKET_WIDTH * BAUD_SAMPLE * BAUD_RATE

uart_tx's clock > PACKET_WIDTH * BAUD_RATE

It is recommended to choose clocks that are at least two times faster, otherwise UART modules will not function properly.

# Schematics

<img width="100%" src="images/uart.svg">

# References

### This project was inspired by the UART Rx Module described in chapter 20 of [Ricardo Jasinski's Effective Coding with VHDL](https://mitpress.mit.edu/books/effective-coding-vhdl)
