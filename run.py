from pathlib import Path
from vunit import VUnit

VU = VUnit.from_argv()
VU.add_verification_components()
VU.enable_location_preprocessing()


SRC_PATH = Path(__file__).parent / "src"
TESTBENCH_PATH = Path(__file__).parent / "testbench"

VU.add_library("uart").add_source_files(SRC_PATH / "*.vhdl")
VU.add_library("uart_tb").add_source_files(TESTBENCH_PATH / "*.vhd")
VU.main()
