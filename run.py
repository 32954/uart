from pathlib import Path
from vunit import VUnit

VU = VUnit.from_argv(compile_builtins=False)
VU.add_vhdl_builtins()


SRC_PATH = Path(__file__).parent / "src"
TESTBENCH_PATH = Path(__file__).parent / "tb"

VU.add_library("uart").add_source_files(SRC_PATH / "**" / "*.vhd")
VU.add_library("uart_tb").add_source_files(TESTBENCH_PATH / "**" / "*.vhdl")

VU.set_compile_option("ghdl.a_flags",["--std=08"])
VU.set_sim_option("ghdl.elab_flags",["--std=08"])

VU.main()
