library ieee;
use ieee.std_logic_1164.all;

library uart;
use uart.uart_pkg.all;

entity uart_tx is
    generic (
        CLOCK_FREQUENCY : natural; -- Clock Frequency in Hertz
        BAUD_RATE : natural; -- Baud Rate
        DATA_WIDTH : natural; -- Bits in the data frame
        STOP_BITS : natural; -- Number of stop bits
        PARITY_EN : natural; -- 0 for no parity, 1 for parity
        PARITY_MODE : parity_mode_t -- odd, even, mark, space
    );
    port (
        reset_n, clock : in std_logic;
        tx_start : in std_logic; -- Start data transmission
        tx_data : in std_logic_vector((DATA_WIDTH - 1) downto 0); -- Data to be transmitted
        tx_pin : out std_logic; -- TX physical interface output
        tx_done : out std_logic -- Data transmitted (one clock pulse)
    );
end entity;

architecture Behavioral of uart_tx is
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    constant PACKET_WIDTH : natural := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    type fsm_state_type is (idle, active);
    ---------------------------------------------------------------------------
    type tx_state_type is record
        -- FSM state
        state_value : fsm_state_type;
        -- Data to be transmitted
        data : std_logic;
        -- Number of Bits in a UART frame
        nbits : natural range PACKET_WIDTH downto 0;
        -- Signal new data transmitted
        done : std_logic;
    end record;
    ---------------------------------------------------------------------------
    signal tx_state, tx_next_state : tx_state_type;
    ---------------------------------------------------------------------------
    signal baud : std_logic; -- Starting point of a baud
    signal tx_parity : std_logic; -- Calculated parity
begin
    baud_start : process (clock, reset_n) is
        constant BAUD_PERIOD_COUNT : natural := (CLOCK_FREQUENCY/BAUD_RATE);
        variable baud_period_counter : natural range BAUD_PERIOD_COUNT downto 0;
    begin
        if reset_n = '0' then
            baud_period_counter := 0;
            baud <= '0';
        elsif rising_edge(clock) then
            if tx_state.state_value = active then
                if baud_period_counter = BAUD_PERIOD_COUNT then
                    baud <= '1';
                    baud_period_counter := 0;
                else
                    baud <= '0';
                    baud_period_counter := baud_period_counter + 1;
                end if;
            else
                baud_period_counter := 0;
            end if;
        end if;
    end process;
    ---------------------------------------------------------------------------
    -- TX state registers update at each clock and reset_n
    reg_process : process (clock, reset_n) is
    begin
        if reset_n = '0' then
            tx_state.state_value <= idle;
            tx_state.data <= '1';
            tx_state.nbits <= 0;
            tx_state.done <= '0';
        elsif rising_edge(clock) then
            tx_state <= tx_next_state;
        end if;
    end process;
    ---------------------------------------------------------------------------
    -- TX FSM: updates tx_next_state from tx_state and inputs.
    tx_process : process (tx_state, baud, tx_start, tx_data, tx_parity) is
    begin
        case tx_state.state_value is
            when idle =>
                tx_next_state.nbits <= 0;
                tx_next_state.done <= '0';

                if tx_start = '1' then
                    -- UART begins
                    tx_next_state.state_value <= active;
                    tx_next_state.data <= '0';
                else
                    -- keep idle
                    tx_next_state.state_value <= idle;
                    tx_next_state.data <= '1';
                end if;

            when active =>
                tx_next_state <= tx_state;
                ---------------------------------------------------------------
                if baud = '1' then
                    if tx_state.nbits >= DATA_WIDTH then
                        -- TX last bit
                        if tx_state.nbits = (PACKET_WIDTH - 1) then
                            -- back to idle state to wait for next tx_start pulse
                            tx_next_state.state_value <= idle;
                            tx_next_state.done <= '1';
                            ---------------------------------------------------
                            -- Transmit stop bits to TX
                        elsif tx_state.nbits >= (PACKET_WIDTH - (STOP_BITS + 1)) then
                            tx_next_state.data <= '1';-- stop bits
                            ---------------------------------------------------
                        elsif PARITY_EN = 1 then
                            if tx_state.nbits = DATA_WIDTH then
                                tx_next_state.data <= tx_parity;
                            end if;
                        end if;
                        -------------------------------------------------------
                        tx_next_state.nbits <= tx_state.nbits + 1;
                    else
                        -- Transmit tx_data bits
                        tx_next_state.data <= tx_data(tx_state.nbits);
                        tx_next_state.nbits <= tx_state.nbits + 1;
                    end if;
                end if;
        end case;
    end process;
    ---------------------------------------------------------------------------
    -- TX output
    tx_output : process (tx_state, tx_data) is
        variable parity : std_logic;
    begin
        tx_pin <= tx_state.data;
        tx_done <= tx_state.done;
        -----------------------------------------------------------------------
        -- Calculate the parity of tx_data
        if PARITY_EN = 1 then
            if PARITY_MODE = odd then
                parity := '1';
                for i in tx_data'range loop
                    parity := parity xor tx_data(i);
                end loop;

            elsif PARITY_MODE = even then
                parity := '0';
                for i in tx_data'range loop
                    parity := parity xor tx_data(i);
                end loop;

            elsif PARITY_MODE = mark then
                parity := '1';

            elsif PARITY_MODE = space then
                parity := '0';
            end if;
        else
            parity := '0';
        end if;
        -- Transmit calculated parity
        tx_parity <= parity;
    end process;
end architecture;