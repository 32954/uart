library ieee;
use ieee.std_logic_1164.all;

library uart;
use uart.uart_pkg.all;

entity uart_rx is
    generic (
        CLOCK_FREQUENCY : natural; -- Clock Frequency in Hertz
        BAUD_RATE : natural; -- Baud Rate
        BAUD_SAMPLE : natural; -- Samples per Baud
        DATA_WIDTH : natural; -- Bits in the data frame
        STOP_BITS : natural; -- Number of stop bits
        PARITY_EN : natural; -- 0 for no parity, 1 for parity
        PARITY_MODE : parity_mode_t -- odd, even, mark, space
    );
    port (
        reset_n, clock : in std_logic;
        rx_pin : in std_logic; -- RX physical interface input
        rx_data : out std_logic_vector((DATA_WIDTH - 1) downto 0); -- Received data
        rx_parity_error : out std_logic; -- 1 for parity error, 0 for none
        rx_done : out std_logic -- Data received (one clock pulse)
    );
end entity;

architecture Behavioral of uart_rx is
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    constant PACKET_WIDTH : natural := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    type fsm_state_type is (idle, active);
    ---------------------------------------------------------------------------
    type rx_state_t is record
        -- FSM state
        state_value : fsm_state_type;
        -- Samples taken per baud
        sample_count : natural range BAUD_SAMPLE downto 0;
        -- Received data
        data : std_logic_vector((DATA_WIDTH - 1) downto 0);
        -- Number of Bits in a UART frame
        nbits : natural range PACKET_WIDTH downto 0;
        -- Parity
        parity : std_logic;
        -- Signal new data received
        done : std_logic;
    end record;
    ---------------------------------------------------------------------------
    signal rx_state, rx_next_state : rx_state_t;
    ---------------------------------------------------------------------------
    signal sample_pulse : std_logic; -- 1 clock spike at 16x baud rate
begin
    sample_pulse_generator : process (clock, reset_n) is
        constant MAX_COUNT : natural := (CLOCK_FREQUENCY/(BAUD_SAMPLE * BAUD_RATE));
        variable sample_counter : natural range MAX_COUNT downto 0;
    begin
        if reset_n = '0' then
            sample_counter := 0;
            sample_pulse <= '0';
        elsif rising_edge(clock) then
            if sample_counter = MAX_COUNT then
                sample_pulse <= '1';
                sample_counter := 0;
            else
                sample_pulse <= '0';
                sample_counter := sample_counter + 1;
            end if;
        end if;
    end process;
    ---------------------------------------------------------------------------
    -- RX state registers update at each clock and reset_n
    reg_process : process (clock, reset_n) is
    begin
        if reset_n = '0' then
            rx_state.state_value <= idle;
            rx_state.sample_count <= 0;
            rx_state.data <= (others => '0');
            rx_state.nbits <= 0;
            rx_state.parity <= '0';
            rx_state.done <= '0';
        elsif rising_edge(clock) then
            rx_state <= rx_next_state;
        end if;
    end process;
    ---------------------------------------------------------------------------
    -- RX FSM: updates rx_next_state from rx_state and inputs.
    rx_process : process (rx_state, sample_pulse, rx_pin) is
    begin
        case rx_state.state_value is
            when idle =>
                rx_next_state.sample_count <= 0;
                rx_next_state.data <= (others => '0');
                rx_next_state.nbits <= 0;
                rx_next_state.parity <= '0';
                rx_next_state.done <= '0';

                if rx_pin = '0' then
                    -- Start a new data
                    rx_next_state.state_value <= active;
                else
                    -- Keep idle
                    rx_next_state.state_value <= idle;
                end if;

            when active =>
                rx_next_state <= rx_state;
                ---------------------------------------------------------------
                if sample_pulse = '1' then
                    -- Sample next RX bit (at the middle of the sample_count cycle)
                    if rx_state.sample_count = (BAUD_SAMPLE/2) then
                        if rx_state.nbits > DATA_WIDTH then
                            -- Last bit is present on RX
                            if rx_state.nbits = (PACKET_WIDTH - 1) then
                                -- Back to idle state to wait for next start bit
                                rx_next_state.state_value <= idle;
                                rx_next_state.done <= '1';
                                -----------------------------------------------
                                -- Stop bits are present on RX
                            elsif rx_state.nbits = (PACKET_WIDTH - STOP_BITS) then
                                rx_next_state.done <= '0';
                                -----------------------------------------------
                            elsif PARITY_EN = 1 then
                                if rx_state.nbits = (DATA_WIDTH + 1) then
                                    rx_next_state.parity <= rx_pin;
                                end if;
                            end if;
                            ---------------------------------------------------
                            rx_next_state.nbits <= rx_state.nbits + 1;
                        else
                            -- Shift new bit in data
                            rx_next_state.data <= rx_pin & rx_state.data((DATA_WIDTH - 1) downto 1);
                            rx_next_state.nbits <= rx_state.nbits + 1;
                        end if;
                    end if;
                    -----------------------------------------------------------
                    -- Reset at the end of each Baud
                    if rx_state.sample_count = (BAUD_SAMPLE - 1) then
                        rx_next_state.sample_count <= 0;
                    else
                        rx_next_state.sample_count <= rx_state.sample_count + 1;
                    end if;
                end if;
        end case;
    end process;
    ---------------------------------------------------------------------------
    -- RX output
    rx_output : process (rx_state, rx_data) is
        variable rx_parity : std_logic;
    begin
        rx_data <= rx_state.data;
        rx_done <= rx_state.done;
        -----------------------------------------------------------------------
        -- Determine the parity of rx_data
        if PARITY_EN = 1 then
            if PARITY_MODE = odd then
                rx_parity := '1';
                for i in rx_data'range loop
                    rx_parity := rx_parity xor rx_data(i);
                end loop;

            elsif PARITY_MODE = even then
                rx_parity := '0';
                for i in rx_data'range loop
                    rx_parity := rx_parity xor rx_data(i);
                end loop;

            elsif PARITY_MODE = mark then
                rx_parity := '1';

            elsif PARITY_MODE = space then
                rx_parity := '0';
            end if;
        else
            rx_parity := '0';
        end if;
        -- Pulse parity_error with rx_done   -- Parity check
        rx_parity_error <= rx_state.done and (rx_parity xor (rx_state.parity));
    end process;
end architecture;