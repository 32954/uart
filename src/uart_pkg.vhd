library ieee;
use ieee.std_logic_1164.all;

package uart_pkg is
    type parity_mode_t is (odd, even, mark, space);
end package;