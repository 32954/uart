LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

PACKAGE uart_pkg IS
    TYPE parity_mode_t IS (odd, even, mark, space);
    ---------------------------------------------------------------------------
    COMPONENT uart_rx IS
        GENERIC (
            CLOCK_FREQUENCY : NATURAL; -- Clock Frequency in Hertz
            BAUD_RATE : NATURAL; -- Baud Rate
            BAUD_SAMPLE : NATURAL; -- Samples per Baud
            DATA_WIDTH : NATURAL; -- Bits in the data frame
            STOP_BITS : NATURAL; -- Number of stop bits
            PARITY_EN : NATURAL; -- 0 for no parity, 1 for parity
            PARITY_MODE : parity_mode_t -- odd, even, mark, space
        );
        PORT (
            reset_n, clock : IN STD_LOGIC;
            rx_pin : IN STD_LOGIC; -- RX physical interface input
            rx_data : OUT STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0); -- Received data
            rx_parity_error : OUT STD_LOGIC; -- 1 for parity error, 0 for none
            rx_done : OUT STD_LOGIC -- Data received (one clock pulse)
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT uart_tx IS
        GENERIC (
            CLOCK_FREQUENCY : NATURAL; -- Clock Frequency in Hertz
            BAUD_RATE : NATURAL; -- Baud Rate
            DATA_WIDTH : NATURAL; -- Bits in the data frame
            STOP_BITS : NATURAL; -- Number of stop bits
            PARITY_EN : NATURAL; -- 0 for no parity, 1 for parity
            PARITY_MODE : parity_mode_t -- odd, even, mark, space
        );
        PORT (
            reset_n, clock : IN STD_LOGIC;
            tx_start : IN STD_LOGIC; -- Start data transmission
            tx_data : IN STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0); -- Data to be transmitted
            tx_pin : OUT STD_LOGIC; -- TX physical interface output
            tx_done : OUT STD_LOGIC -- Data transferred (one clock pulse)
        );
    END COMPONENT;
END PACKAGE;