LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY uart;
USE uart.uart_pkg.ALL;

ENTITY uart_tx IS
    GENERIC (
        CLOCK_FREQUENCY : NATURAL; -- Clock Frequency in Hertz
        BAUD_RATE : NATURAL; -- Baud Rate
        DATA_WIDTH : NATURAL; -- Bits in the data frame
        STOP_BITS : NATURAL; -- Number of stop bits
        PARITY_EN : NATURAL; -- 0 for no parity, 1 for parity
        PARITY_MODE : parity_mode_t -- odd, even, mark, space
    );
    PORT (
        reset_n, clock : IN STD_LOGIC;
        tx_start : IN STD_LOGIC; -- Start data transmission
        tx_data : IN STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0); -- Data to be transmitted
        tx_pin : OUT STD_LOGIC; -- TX physical interface output
        tx_done : OUT STD_LOGIC -- Data transmitted (one clock pulse)
    );
END ENTITY;

ARCHITECTURE Behavioral OF uart_tx IS
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    CONSTANT PACKET_WIDTH : NATURAL := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    TYPE fsm_state_type IS (idle, active);
    ---------------------------------------------------------------------------
    TYPE tx_state_type IS RECORD
        -- FSM state
        state_value : fsm_state_type;
        -- Data to be transmitted
        data : STD_LOGIC;
        -- Number of Bits in a UART frame
        nbits : NATURAL RANGE PACKET_WIDTH DOWNTO 0;
        -- Signal new data transmitted
        done : STD_LOGIC;
    END RECORD;
    ---------------------------------------------------------------------------
    SIGNAL tx_state, tx_next_state : tx_state_type;
    ---------------------------------------------------------------------------
    SIGNAL baud : STD_LOGIC; -- Starting point of a baud
    SIGNAL tx_parity : STD_LOGIC; -- Calculated parity
BEGIN

    baud_start : PROCESS (clock, reset_n) IS
        CONSTANT BAUD_PERIOD_COUNT : NATURAL := (CLOCK_FREQUENCY/BAUD_RATE);
        VARIABLE baud_period_counter : NATURAL RANGE BAUD_PERIOD_COUNT DOWNTO 0;
    BEGIN
        IF reset_n = '0' THEN
            baud_period_counter := 0;
            baud <= '0';
        ELSIF rising_edge(clock) THEN
            IF tx_state.state_value = active THEN
                IF baud_period_counter = BAUD_PERIOD_COUNT THEN
                    baud <= '1';
                    baud_period_counter := 0;
                ELSE
                    baud <= '0';
                    baud_period_counter := baud_period_counter + 1;
                END IF;
            ELSE
                baud_period_counter := 0;
            END IF;
        END IF;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- TX state registers update at each clock and reset_n
    reg_process : PROCESS (clock, reset_n) IS
    BEGIN
        IF reset_n = '0' THEN
            tx_state.state_value <= idle;
            tx_state.data <= '1';
            tx_state.nbits <= 0;
            tx_state.done <= '0';
        ELSIF rising_edge(clock) THEN
            tx_state <= tx_next_state;
        END IF;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- TX FSM: updates tx_next_state from tx_state and inputs.
    tx_process : PROCESS (tx_state, baud, tx_start, tx_data, tx_parity) IS
    BEGIN
        CASE tx_state.state_value IS
            WHEN idle =>
                tx_next_state.nbits <= 0;
                tx_next_state.done <= '0';

                IF tx_start = '1' THEN
                    -- UART begins
                    tx_next_state.state_value <= active;
                    tx_next_state.data <= '0';
                ELSE
                    -- keep idle
                    tx_next_state.state_value <= idle;
                    tx_next_state.data <= '1';
                END IF;

            WHEN active =>
                tx_next_state <= tx_state;
                ---------------------------------------------------------------
                IF baud = '1' THEN
                    IF tx_state.nbits >= DATA_WIDTH THEN
                        -- TX last bit
                        IF tx_state.nbits = (PACKET_WIDTH - 1) THEN
                            -- back to idle state to wait for next tx_start pulse
                            tx_next_state.state_value <= idle;
                            tx_next_state.done <= '1';
                            ---------------------------------------------------
                            -- Transmit stop bits to TX
                        ELSIF tx_state.nbits >= (PACKET_WIDTH - (STOP_BITS + 1)) THEN
                            tx_next_state.data <= '1';-- stop bits
                            ---------------------------------------------------
                        ELSIF PARITY_EN = 1 THEN
                            IF tx_state.nbits = DATA_WIDTH THEN
                                tx_next_state.data <= tx_parity;
                            END IF;
                        END IF;
                        -------------------------------------------------------
                        tx_next_state.nbits <= tx_state.nbits + 1;
                    ELSE
                        -- Transmit tx_data bits
                        tx_next_state.data <= tx_data(tx_state.nbits);
                        tx_next_state.nbits <= tx_state.nbits + 1;
                    END IF;
                END IF;
        END CASE;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- TX output
    tx_output : PROCESS (tx_state, tx_data) IS
        VARIABLE parity : STD_LOGIC;
    BEGIN
        tx_pin <= tx_state.data;
        tx_done <= tx_state.done;
        -----------------------------------------------------------------------
        -- Calculate the parity of tx_data
        IF PARITY_EN = 1 THEN
            IF PARITY_MODE = odd THEN
                parity := '1';
                FOR i IN tx_data'RANGE LOOP
                    parity := parity XOR tx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = even THEN
                parity := '0';
                FOR i IN tx_data'RANGE LOOP
                    parity := parity XOR tx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = mark THEN
                parity := '1';

            ELSIF PARITY_MODE = space THEN
                parity := '0';
            END IF;
        ELSE
            parity := '0';
        END IF;
        -- Transmit calculated parity
        tx_parity <= parity;
    END PROCESS;
END ARCHITECTURE;