LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY uart;
USE uart.uart_pkg.ALL;

ENTITY uart_rx IS
    GENERIC (
        CLOCK_FREQUENCY : NATURAL; -- Clock Frequency in Hertz
        BAUD_RATE : NATURAL; -- Baud Rate
        BAUD_SAMPLE : NATURAL; -- Samples per Baud
        DATA_WIDTH : NATURAL; -- Bits in the data frame
        STOP_BITS : NATURAL; -- Number of stop bits
        PARITY_EN : NATURAL; -- 0 for no parity, 1 for parity
        PARITY_MODE : parity_mode_t -- odd, even, mark, space
    );
    PORT (
        reset_n, clock : IN STD_LOGIC;
        rx_pin : IN STD_LOGIC; -- RX physical interface input
        rx_data : OUT STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0); -- Received data
        rx_parity_error : OUT STD_LOGIC; -- 1 for parity error, 0 for none
        rx_done : OUT STD_LOGIC -- Data received (one clock pulse)
    );
END ENTITY;

ARCHITECTURE Behavioral OF uart_rx IS
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    CONSTANT PACKET_WIDTH : NATURAL := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    TYPE fsm_state_type IS (idle, active);
    ---------------------------------------------------------------------------
    TYPE rx_state_t IS RECORD
        -- FSM state
        state_value : fsm_state_type;
        -- Samples taken per baud
        sample_count : NATURAL RANGE BAUD_SAMPLE DOWNTO 0;
        -- Received data
        data : STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
        -- Number of Bits in a UART frame
        nbits : NATURAL RANGE PACKET_WIDTH DOWNTO 0;
        -- Parity
        parity : STD_LOGIC;
        -- Signal new data received
        done : STD_LOGIC;
    END RECORD;
    ---------------------------------------------------------------------------
    SIGNAL rx_state, rx_next_state : rx_state_t;
    ---------------------------------------------------------------------------
    SIGNAL sample_pulse : STD_LOGIC; -- 1 clock spike at 16x baud rate

BEGIN

    sample_pulse_generator : PROCESS (clock, reset_n) IS
        CONSTANT MAX_COUNT : NATURAL := (CLOCK_FREQUENCY/(BAUD_SAMPLE * BAUD_RATE));
        VARIABLE sample_counter : NATURAL RANGE MAX_COUNT DOWNTO 0;
    BEGIN
        IF reset_n = '0' THEN
            sample_counter := 0;
            sample_pulse <= '0';
        ELSIF rising_edge(clock) THEN
            IF sample_counter = MAX_COUNT THEN
                sample_pulse <= '1';
                sample_counter := 0;
            ELSE
                sample_pulse <= '0';
                sample_counter := sample_counter + 1;
            END IF;
        END IF;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- RX state registers update at each clock and reset_n
    reg_process : PROCESS (clock, reset_n) IS
    BEGIN
        IF reset_n = '0' THEN
            rx_state.state_value <= idle;
            rx_state.sample_count <= 0;
            rx_state.data <= (OTHERS => '0');
            rx_state.nbits <= 0;
            rx_state.parity <= '0';
            rx_state.done <= '0';
        ELSIF rising_edge(clock) THEN
            rx_state <= rx_next_state;
        END IF;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- RX FSM: updates rx_next_state from rx_state and inputs.
    rx_process : PROCESS (rx_state, sample_pulse, rx_pin) IS
    BEGIN
        CASE rx_state.state_value IS
            WHEN idle =>
                rx_next_state.sample_count <= 0;
                rx_next_state.data <= (OTHERS => '0');
                rx_next_state.nbits <= 0;
                rx_next_state.parity <= '0';
                rx_next_state.done <= '0';

                IF rx_pin = '0' THEN
                    -- Start a new data
                    rx_next_state.state_value <= active;
                ELSE
                    -- Keep idle
                    rx_next_state.state_value <= idle;
                END IF;

            WHEN active =>
                rx_next_state <= rx_state;
                ---------------------------------------------------------------
                IF sample_pulse = '1' THEN
                    -- Sample next RX bit (at the middle of the sample_count cycle)
                    IF rx_state.sample_count = (BAUD_SAMPLE/2) THEN
                        IF rx_state.nbits > DATA_WIDTH THEN
                            -- Last bit is present on RX
                            IF rx_state.nbits = (PACKET_WIDTH - 1) THEN
                                -- Back to idle state to wait for next start bit
                                rx_next_state.state_value <= idle;
                                rx_next_state.done <= '1';
                                -----------------------------------------------
                                -- Stop bits are present on RX
                            ELSIF rx_state.nbits = (PACKET_WIDTH - STOP_BITS) THEN
                                rx_next_state.done <= '0';
                                -----------------------------------------------
                            ELSIF PARITY_EN = 1 THEN
                                IF rx_state.nbits = (DATA_WIDTH + 1) THEN
                                    rx_next_state.parity <= rx_pin;
                                END IF;
                            END IF;
                            ---------------------------------------------------
                            rx_next_state.nbits <= rx_state.nbits + 1;
                        ELSE
                            -- Shift new bit in data
                            rx_next_state.data <= rx_pin & rx_state.data((DATA_WIDTH - 1) DOWNTO 1);
                            rx_next_state.nbits <= rx_state.nbits + 1;
                        END IF;
                    END IF;
                    -----------------------------------------------------------
                    -- Reset at the end of each Baud
                    IF rx_state.sample_count = (BAUD_SAMPLE - 1) THEN
                        rx_next_state.sample_count <= 0;
                    ELSE
                        rx_next_state.sample_count <= rx_state.sample_count + 1;
                    END IF;
                END IF;
        END CASE;
    END PROCESS;
    ---------------------------------------------------------------------------
    -- RX output
    rx_output : PROCESS (rx_state, rx_data) IS
        VARIABLE rx_parity : STD_LOGIC;
    BEGIN
        rx_data <= rx_state.data;
        rx_done <= rx_state.done;
        -----------------------------------------------------------------------
        -- Determine the parity of rx_data
        IF PARITY_EN = 1 THEN
            IF PARITY_MODE = odd THEN
                rx_parity := '1';
                FOR i IN rx_data'RANGE LOOP
                    rx_parity := rx_parity XOR rx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = even THEN
                rx_parity := '0';
                FOR i IN rx_data'RANGE LOOP
                    rx_parity := rx_parity XOR rx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = mark THEN
                rx_parity := '1';

            ELSIF PARITY_MODE = space THEN
                rx_parity := '0';
            END IF;
        ELSE
            rx_parity := '0';
        END IF;
        -- Pulse parity_error with rx_done   -- Parity check
        rx_parity_error <= rx_state.done AND (rx_parity XOR (rx_state.parity));
    END PROCESS;
END ARCHITECTURE;