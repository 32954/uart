LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

LIBRARY uart;
USE uart.uart_pkg.ALL;

ENTITY uart_rx_parity_tb IS
    GENERIC (runner_cfg : STRING);
    CONSTANT CLOCK_FREQUENCY : NATURAL := 100_000_000;
    CONSTANT BAUD_RATE : NATURAL := 115_200;
    CONSTANT BAUD_SAMPLE : NATURAL := 16;
    CONSTANT DATA_WIDTH : NATURAL := 8;
    CONSTANT STOP_BITS : NATURAL := 1;
    CONSTANT PARITY_EN : NATURAL := 1;
    CONSTANT PARITY_MODE : parity_mode_t := odd;
END ENTITY;

ARCHITECTURE testbench OF uart_rx_parity_tb IS
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    CONSTANT PACKET_WIDTH : NATURAL := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    CONSTANT BAUD_PERIOD : TIME := (1.0/real(BAUD_RATE)) * 1 sec;
    CONSTANT PACKET_PERIOD : TIME := PACKET_WIDTH * BAUD_PERIOD;
    ---------------------------------------------------------------------------
    SIGNAL reset_n : STD_LOGIC;
    SIGNAL clock : STD_LOGIC := '0';
    SIGNAL rx_pin : STD_LOGIC := '1';
    SIGNAL rx_data : STD_LOGIC_VECTOR(DATA_WIDTH - 1 DOWNTO 0);
    SIGNAL rx_parity_error : STD_LOGIC;
    SIGNAL rx_done : STD_LOGIC;
    ---------------------------------------------------------------------------
    SIGNAL rx_data_expected : STD_LOGIC_VECTOR(DATA_WIDTH - 1 DOWNTO 0);
    SIGNAL transaction_done : STD_LOGIC;
    SIGNAL parity_signal : STD_LOGIC;
BEGIN
    ---------------------------- DUT Instantiation ----------------------------
    dut : uart_rx GENERIC MAP(
        CLOCK_FREQUENCY => CLOCK_FREQUENCY,
        BAUD_RATE => BAUD_RATE,
        BAUD_SAMPLE => BAUD_SAMPLE,
        DATA_WIDTH => DATA_WIDTH,
        STOP_BITS => STOP_BITS,
        PARITY_EN => PARITY_EN,
        PARITY_MODE => PARITY_MODE
    )
    PORT MAP(
        reset_n => reset_n,
        clock => clock,
        rx_pin => rx_pin,
        rx_data => rx_data,
        rx_parity_error => rx_parity_error,
        rx_done => rx_done
    );
    --------------------------- Stimuli Generation ----------------------------
    clock <= NOT clock AFTER ((1.0/real(CLOCK_FREQUENCY * 2)) * 1 sec);
    reset_n <= '0', '1' AFTER 20 ns;

    stimuli_generator : PROCESS BEGIN
        WAIT UNTIL reset_n;

        -- Send data from 0 - ((2 ** DATA_WIDTH) - 1), one at a time
        FOR i IN 0 TO ((2 ** DATA_WIDTH) - 1) LOOP
            -- Writing the rx_data_expected signal begins the driver process
            rx_data_expected <= STD_LOGIC_VECTOR(to_unsigned(i, DATA_WIDTH));
            -- Wait until the driver process has finished transmitting
            WAIT ON transaction_done'transaction;
        END LOOP;
    END PROCESS;

    -- Transmitting the value of rx_data_expected to rx_pin
    driver : PROCESS
        VARIABLE tx_buffer : STD_LOGIC_VECTOR((PACKET_WIDTH - 1) DOWNTO 0);
        VARIABLE stop_signal : STD_LOGIC_VECTOR((STOP_BITS - 1) DOWNTO 0) := (OTHERS => '1');
    BEGIN
        WAIT ON parity_signal'transaction;

        -- Add start and stop and parity bits to rx_data_expected
        tx_buffer := stop_signal & parity_signal & rx_data_expected & '0';
        FOR i IN tx_buffer'reverse_range LOOP
            rx_pin <= tx_buffer(i);
            WAIT FOR BAUD_PERIOD;
        END LOOP;
        transaction_done <= '1';
    END PROCESS;

    parity_generator : PROCESS
        VARIABLE rx_parity : STD_LOGIC;
    BEGIN
        WAIT ON rx_data_expected'transaction;

        IF PARITY_EN = 1 THEN
            IF PARITY_MODE = odd THEN
                rx_parity := '1';
                FOR i IN rx_data_expected'RANGE LOOP
                    rx_parity := rx_parity XOR rx_data_expected(i);
                END LOOP;

            ELSIF PARITY_MODE = even THEN
                rx_parity := '0';
                FOR i IN rx_data_expected'RANGE LOOP
                    rx_parity := rx_parity XOR rx_data_expected(i);
                END LOOP;

            ELSIF PARITY_MODE = mark THEN
                rx_parity := '1';

            ELSIF PARITY_MODE = space THEN
                rx_parity := '0';
            END IF;
            parity_signal <= rx_parity;
        ELSE
            parity_signal <= '0';
        END IF;
    END PROCESS;
    ---------------------------------- Check ----------------------------------
    test_runner : PROCESS
    BEGIN
        test_runner_setup(runner, runner_cfg);

        WAIT UNTIL reset_n;
        FOR i IN 0 TO ((2 ** DATA_WIDTH) - 1) LOOP
            WAIT UNTIL rx_done;
            -- Check that all values of rx_data are tested
            check_equal(rx_data, i, "Didn't test all values");
            -- Check for parity error
            check_equal(rx_parity_error, '0', "parity error");
            -- Compare the actual and expected values of the rx_data
            check_equal(rx_data, rx_data_expected, "rx_data is " & to_string(rx_data));
        END LOOP;

        info("End of testbench. All tests passed.");
        test_runner_cleanup(runner);
    END PROCESS;
    -- Set it to a higher value if you get timeout error
    test_runner_watchdog(runner, BAUD_PERIOD * PACKET_WIDTH * ((2 ** DATA_WIDTH) + 1));
END ARCHITECTURE;