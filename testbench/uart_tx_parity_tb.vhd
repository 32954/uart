LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

LIBRARY uart;
USE uart.uart_pkg.ALL;

ENTITY uart_tx_parity_tb IS
    GENERIC (runner_cfg : STRING);
    CONSTANT CLOCK_FREQUENCY : NATURAL := 100_000_000;
    CONSTANT BAUD_RATE : NATURAL := 115_200;
    CONSTANT DATA_WIDTH : NATURAL := 8;
    CONSTANT STOP_BITS : NATURAL := 1;
    CONSTANT PARITY_EN : NATURAL := 1;
    CONSTANT PARITY_MODE : parity_mode_t := odd;
END ENTITY;

ARCHITECTURE testbench OF uart_tx_parity_tb IS
    --       PACKET WIDTH =    START BIT + DATA_WIDTH + PARITY_EN + STOP_BITS;
    CONSTANT PACKET_WIDTH : NATURAL := 1 + DATA_WIDTH + PARITY_EN + STOP_BITS;
    ---------------------------------------------------------------------------
    CONSTANT BAUD_PERIOD : TIME := (1.0/real(BAUD_RATE)) * 1 sec;
    CONSTANT PACKET_PERIOD : TIME := PACKET_WIDTH * BAUD_PERIOD;
    ---------------------------------------------------------------------------
    SIGNAL reset_n : STD_LOGIC;
    SIGNAL clock : STD_LOGIC := '0';
    SIGNAL tx_start : STD_LOGIC := '1';
    SIGNAL tx_data : STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
    SIGNAL tx_pin : STD_LOGIC;
    SIGNAL tx_done : STD_LOGIC;
    ---------------------------------------------------------------------------
    -- Preserves previous tx_data before changing
    SIGNAL previous_tx_data : STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
    SIGNAL tx_data_expected : STD_LOGIC_VECTOR((PACKET_WIDTH - 1) DOWNTO 0);
    SIGNAL transaction_done : STD_LOGIC;
    SIGNAL parity_signal : STD_LOGIC;
BEGIN
    ---------------------------- DUT Instantiation ----------------------------
    dut : uart_tx GENERIC MAP(
        CLOCK_FREQUENCY => CLOCK_FREQUENCY,
        BAUD_RATE => BAUD_RATE,
        DATA_WIDTH => DATA_WIDTH,
        STOP_BITS => STOP_BITS,
        PARITY_EN => PARITY_EN,
        PARITY_MODE => PARITY_MODE
    )
    PORT MAP(
        reset_n => reset_n,
        clock => clock,
        tx_start => tx_start,
        tx_data => tx_data,
        tx_pin => tx_pin,
        tx_done => tx_done
    );
    --------------------------- Stimuli Generation ----------------------------
    clock <= NOT clock AFTER ((1.0/real(CLOCK_FREQUENCY * 2)) * 1 sec);
    reset_n <= '0', '1' AFTER 10 ns;
    tx_start <= NOT tx_start AFTER (PACKET_PERIOD/2);

    stimuli_generator : PROCESS BEGIN
        WAIT UNTIL reset_n;

        -- Send data from 0 - ((2 ** DATA_WIDTH) - 1), one at a time
        FOR i IN 0 TO ((2 ** DATA_WIDTH) - 1) LOOP
            -- Writing the tx_data signal begins the driver process
            tx_data <= STD_LOGIC_VECTOR(to_unsigned(i, DATA_WIDTH));
            WAIT UNTIL tx_done;
        END LOOP;
    END PROCESS;

    -- Store data from the tx_pin
    driver : PROCESS
        VARIABLE expected : STD_LOGIC_VECTOR((PACKET_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
    BEGIN
        WAIT ON tx_data'transaction;
        WAIT UNTIL tx_pin = '0';
        -- Preserves tx_data
        previous_tx_data <= tx_data;

        FOR i IN 0 TO PACKET_WIDTH - 1 LOOP
            -- Shift tx_pin in expected
            expected := tx_pin & expected((PACKET_WIDTH - 1) DOWNTO 1);
            WAIT ON tx_pin'transaction;
        END LOOP;

        tx_data_expected <= expected;
        transaction_done <= '1';
    END PROCESS;

    parity_generator : PROCESS
        VARIABLE tx_parity : STD_LOGIC;
    BEGIN
        WAIT ON tx_data'transaction;

        IF PARITY_EN = 1 THEN
            IF PARITY_MODE = odd THEN
                tx_parity := '1';
                FOR i IN tx_data'RANGE LOOP
                    tx_parity := tx_parity XOR tx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = even THEN
                tx_parity := '0';
                FOR i IN tx_data'RANGE LOOP
                    tx_parity := tx_parity XOR tx_data(i);
                END LOOP;

            ELSIF PARITY_MODE = mark THEN
                tx_parity := '1';

            ELSIF PARITY_MODE = space THEN
                tx_parity := '0';
            END IF;
            parity_signal <= tx_parity;
        ELSE
            parity_signal <= '0';
        END IF;
    END PROCESS;
    ---------------------------------- Check ----------------------------------
    test_runner : PROCESS
        VARIABLE stop_bits_value : STD_LOGIC_VECTOR(STOP_BITS - 1 DOWNTO 0) := (OTHERS => '1');
    BEGIN
        test_runner_setup(runner, runner_cfg);

        WAIT UNTIL reset_n;
        FOR i IN 0 TO ((2 ** DATA_WIDTH) - 1) LOOP
            WAIT ON transaction_done'transaction;
            -- Check that all values of tx_data are tested
            check_equal(previous_tx_data, i, "Didn't test all values");
            -- Print the UART frame on the screen
            print("start: " & to_string((tx_data_expected(0))) & "    " &

            "stop: " & to_string(
            tx_data_expected((PACKET_WIDTH - 1) DOWNTO (PACKET_WIDTH - STOP_BITS))) & "    " &

            to_string(PARITY_MODE) & "_parity: " & to_string(tx_data_expected(DATA_WIDTH + 1)) & ht &

            "data: " & to_string(tx_data_expected(DATA_WIDTH DOWNTO 1)) & " : " &
            to_string(to_integer(unsigned((tx_data_expected(DATA_WIDTH DOWNTO 1)))))
            );
            -- Check start value
            check_equal(tx_data_expected(0), '0', "Wrong Start value");
            -- Compare the actual and expected values of the tx_pin 
            check_equal(tx_data_expected(DATA_WIDTH DOWNTO 1), previous_tx_data,
            "tx_data is " & to_string(previous_tx_data));
            -- Check for parity error
            check_equal(tx_data_expected(DATA_WIDTH + 1), parity_signal, "Wrong parity");
            -- Check correct stop bits value
            check_equal(tx_data_expected((PACKET_WIDTH - 1) DOWNTO (PACKET_WIDTH - STOP_BITS)),
            stop_bits_value, "wrong stop bit value");
        END LOOP;

        info("End of testbench. All tests passed.");
        test_runner_cleanup(runner);
    END PROCESS;
    -- Set it to a higher value if you get timeout error
    test_runner_watchdog(runner, PACKET_PERIOD * ((2 ** DATA_WIDTH) + 1));
END ARCHITECTURE;